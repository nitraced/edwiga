
// Test purposes
#define TEST_ON_UNO_CHIP 0

#if TEST_ON_UNO_CHIP
  #define PIN_HIGH_SCORE 2
  #define PIN_DIGIT1 3
  #define PIN_DIGIT2 4
  #define PIN_DIGIT3 5
  #define PIN_LDR A0
#else 
#define PIN_HIGH_SCORE 3
#define PIN_DIGIT1 2
#define PIN_DIGIT2 0
#define PIN_DIGIT3 1
#define PIN_LDR A2
#endif 

#define HIGH_SCORE_ADDR 0


// Diodes are reversed
const char LED_ADDR[8] ={3,1,4,2,0,7,5,6};
// Curent position
static char i = 4;
// Time between two poses
static short dt = 1000;
// Decrement between two levels
const short delta = 10; 
// Lives counter
static char lives = 4;
// Level/score counter
static uint32_t lvl = 0;
// Luminosity threshold 
static int32_t tresh;

void send_number(char i){
  // Send a number 
  char to_send = LED_ADDR[i];
  // Debugged
  digitalWrite(PIN_DIGIT1,to_send & 0b1);
  digitalWrite(PIN_DIGIT2,(to_send>>1) & 0b1);
  digitalWrite(PIN_DIGIT3,(to_send>>2) & 0b1);
}

void display_score(uint32_t score){
  // Display your score 
for(char j=0; j<=score; j++){
      send_number(j%8);
      delay(500);
    }
}

void lost(){
   // Boom 
   lives--;
   // You won't last long
   send_number(lives);
   // Beep Beep Beep Beep how infortunate
  for(char j=0; j<10; j++){
      digitalWrite(PIN_HIGH_SCORE,HIGH);
      delay(100);
      digitalWrite(PIN_HIGH_SCORE,LOW);
      delay(100);
  }
  // A little rest to weep your tears 
  digitalWrite(PIN_HIGH_SCORE,HIGH);
  delay(1000);
  
  digitalWrite(PIN_HIGH_SCORE,LOW);
  i=4;
}

void win(){
  // Lvl UP 
  lvl++;
  // Toughness UP  
  dt-=delta;
  // Beep Beep Beep ur on the win
  for(char j=0; j<3; j++){
    digitalWrite(PIN_HIGH_SCORE,HIGH);
    delay(100);
    digitalWrite(PIN_HIGH_SCORE,LOW);
    delay(100);
  }
}



void setup() {
  // All outputs are outputs but the input is a default output 
  pinMode(PIN_DIGIT1,OUTPUT);
  pinMode(PIN_DIGIT2,OUTPUT);
  pinMode(PIN_DIGIT3,OUTPUT);
  pinMode(PIN_HIGH_SCORE,OUTPUT);

  // The game has not started yet
  digitalWrite(PIN_HIGH_SCORE,HIGH); 
  delay(1000);
  // Led calibration during ~4.8 seconds 
  for(char iterate=0; iterate<48;iterate++){
   send_number(iterate%8);
   tresh += analogRead(PIN_LDR);
   delay(100);
  }
  // Mean of the points 
  tresh/=48;
  digitalWrite(PIN_HIGH_SCORE,LOW);  
  // Ready??
  delay(1000);
}


void loop() {
  send_number(i);
  if(dt < 100){
    // Avoid too tough levels 
    dt = 100; 
  }
  // Transition delay between two diodes 
  delay(dt);  
  // Depending on the light                                                                              
  if(analogRead(PIN_LDR)>tresh){
    if(i==7){
     // Lost at the left 
      lost();
    }
    else{
      if(i==0){
        win(); // You win a point (left)
      }
      i++; // Going left 
    }
  }
  else{
     if(i==0){
      // Lost at the right 
      lost();
    }
    else{
      if(i==7){
        win(); // You win a point (right)
      }
      i--; // Going right 
    }
  }

// Game over
  if(lives==0){
    digitalWrite(PIN_HIGH_SCORE,HIGH);
    // Reset the game
    dt=1000;
    lives = 4;
    // Display final score
    display_score(lvl);
    // 5 s to admire your tremendous results
    delay(5000);
    // Game again!
    digitalWrite(PIN_HIGH_SCORE,LOW);
  } 

}
