

# Edwiga Tiny Pong game

## Présentation et règles du jeu

## <img src="box.jpg" alt="box" style="zoom:20%;" />

### Contrôle du pong

Allumer le jeu avec le bouton, après une seconde, ce dernier entrera en mode calibration. Une série de mesure sera prise pendant cinq secondes par le capteur de luminosité afin de déterminer le seuil. Durant le procédé (lorsque la diode active du chenillard défile rapidement), **ne pas bouger votre main au dessus du capteur**! Ensuite, vous pouvez jouer. 

**Le seuil est la clé de ce jeu**. En dessous de ce dernier (*i.e.*, en obscurcissant en approchant votre main) la diode allumée va à droite, au dessus à gauche. 

<u>Conseil :</u> choisir un seuil à 20 cm au dessus du boîtier avec toute la paume de la main qui vient ombrager le capteur. Choisir un lieu à éclairement constant et éviter les petits malins qui brouillent le signal (par exemple, en vous envoyant de la lumière avec leur smartphones pour vous faire perdre!).  

### Marquer un point

Pour marquer un point, il suffit de rebrousser chemin <u>au niveau des diodes rouges</u> (les diodes à l'extrémité du chenillard). Attention toutefois, si la diode active continue son chemin... on perd!

Un point en plus est indiqué par trois clignotements brefs de la diode de score. **Attention** le jeu reprend aussitôt et on peut perdre un point si on se trompe de sens d'avance de la diode! 

### Diode de score

Lorsqu'elle est allumée, le jeu est en pause. 

- Si elle clignote, un point est ôté (dix clignotements) ou ajouté (trois brefs). 
- Si elle s'allume continuellement, on affiche le score ou le nombre de vies.

### Barre de vie

À chaque point perdu, une vie est ôtée. Quatre vies sont donnés au départ de chaque partie. Dès qu'on perd un point, la diode de score clignote dix fois et ensuite, le nombre de vie est affiché sur la partie droite du chenillard.

| Diode    | Nombre de vies restantes                   |
| -------- | ------------------------------------------ |
| 2e verte | 3... ça arrive, on croit en vous!          |
| 1e verte | 2... méfiez-vous, vous devenez pas bon!    |
| jaune    | 1... vous êtes mauvais, mais on vous aime! |
| rouge    | 0... l'épée de Damoclès est sur vous!      |

## Game over

Si vous perdez une vie après zéro, vous perdez la partie! 

La diode de score s'allume alors continuellement et... la diode active du chenillard défile à vive allure. Cette diode s'arrêtera sur votre score. Le nombre de fois où la diode reboucle est un multiple de huit, donc ... rappel

| n      | 1    | 2    | 3    | 4    | 5    | 6    | 7    | 8    | 9    | 10   |
| ------ | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| **8n** | 8    | 16   | 24   | 32   | 40   | 48   | 56   | 64   | 72   | 80   |

Enjoy,

Justin.
